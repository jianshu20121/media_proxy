# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tdwl/work/media_proxy/tdCodec/src/TdH264.cpp" "/home/tdwl/work/media_proxy/build/tdCodec/CMakeFiles/codec.dir/src/TdH264.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/tdwl/ffmpeg_lib_n4.4/include"
  "/usr/local/include/jrtplib3"
  "/usr/local/include/wels"
  "/usr/local/include"
  "../tdCodec"
  "../tdScale"
  "../tdServer"
  "../tdPs"
  "../tdConf"
  "../tdUtil"
  "../tdGb28181"
  "../tdUtil/inc"
  "../tdCodec/."
  "../tdCodec/inc"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/tdwl/work/media_proxy/build/tdScale/CMakeFiles/scale.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
